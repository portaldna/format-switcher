<?php

/**
 * @file
 * Admin page callbacks for Format Switcher.
 *
 * @todo do we need to be more granular (which fields, entity types, etc.)
 * with switching?
 * @todo do we need to report on each entity/block?
 */

/**
 * Form: Format Switcher Switcher.
 */
function format_switcher_switcher_form($form, &$form_state) {
  $formats_in_use = format_switcher_get_text_formats_in_use();
  $no_format_option = _format_switcher_no_format_option();
  $formats_enabled = format_switcher_filter_formats(TRUE);

  $form['info'] = [
    'description' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('The Text Format Switcher module attempts to identify everywhere a text format may be in use and then allow an administrator to change that format to another enabled format. When switching to another format, the -format- column in the database is changed without saving the object.'),
    ],
    'recommended_modules' => [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => t('This module does not attempt to evaluate the content or detail which specific objects are using a particular text format. Refer to the !security or !php_finder modules for additional capabilities.', [
        '!security' => l(t('Security Review'), 'https://www.drupal.org/project/security_review', [
          'external' => TRUE,
        ]),
        '!php_finder' => l(t('PHP Finder'), 'https://www.drupal.org/project/php_finder', [
          'external' => TRUE,
        ]),
      ]),
    ],
  ];

  $form['old_formats'] = [
    '#title' => t('Text Formats In-Use'),
    '#type' => 'select',
    '#description' => t('Select a format in-use to change.'),
    '#options' => $formats_in_use,
    '#multiple' => TRUE,
  ];

  $form['new_format'] = [
    '#title' => t('Change to Format'),
    '#type' => 'select',
    '#description' => t('Select an enabled text format.'),
    '#options' => $no_format_option + $formats_enabled,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Switch'),
  ];

  return $form;
}

/**
 * Submit handler: Format Switcher Switcher.
 */
function format_switcher_switcher_form_submit($form, &$form_state) {
  $old_formats = $form_state['values']['old_formats'];
  $new_format = $form_state['values']['new_format'];

  $formattable_fields = format_switcher_get_formattable_fields();

  $field_count = format_switcher_switch_fields($formattable_fields, $old_formats, $new_format);
  $block_count = format_switcher_switch_block($old_formats, $new_format);
  $webform_coount = format_switcher_switch_webform($old_formats, $new_format);

  drupal_set_message(t('Number of field formats changed: !field_count<br/>Number of block format values changed: !block_count', [
    '!field_count' => $field_count,
    '!block_count' => $block_count,
  ]));
}
